#include <iostream>
#include "trie.h"

Node Node::Empty;

Node::Node() {
	for (int i = 0; i < letterCount; ++i)
		children[i] = 0;
}

Node::~Node() {
	for (int i = 0; i < letterCount - 1; ++i)
		delete children[i];
}

void Node::add(const char* word) {
	if (word[0] == '\0') {
		children[Node::letterCount - 1] = &Node::Empty;
	}
	else {
		int code = word[0] - 'a';
		
		if (code >= 0 && code < letterCount - 1) {
			Node* next = children[code];
			if (!next) {
				next = new Node();
				children[code] = next;
			}
			next->add(word + 1);
		}
	}
}

void Node::remove(const char* word) {
	if (word[0] == '\0') {
		children[Node::letterCount - 1] = 0;
	}
	else {
		int code = word[0] - 'a';
		if (code >= 0 && code < letterCount - 1) {
			Node* next = children[code];
			if (!next)
				return;
			next->remove(word + 1);
			// ustalimy czy usunąć podwęzeł
			bool removeNext = true;
			// tylko gdy wszystko pod nim jest puste
			for (int i = 0; i < Node::letterCount && removeNext; ++i)
				removeNext = removeNext && (next->children[i] == 0);
			if (removeNext) {
				children[code] = 0;
				delete next;
			}
		}
	}
}

void Node::print() {
	print(0);
}

void printChar(char c, int indent) {
	for (int i = 0; i < indent; ++i)
		std::cout << "  ";
	std::cout << c << std::endl;
}


void Node::print(int indent) {
	if (children[Node::letterCount - 1]) {
		printChar('.', indent);
	}
	for (int i = 0; i < Node::letterCount - 1; ++i) {
		Node* next = children[i];
		if (next) {
			printChar('a' + i, indent);
			next->print(indent + 1);
		}
	}
}

bool Node::search(const char* word) {
	if (*word == '\0')
		return children[Node::letterCount - 1];
	else {
		int code = word[0] - 'a';
		if (code >= 0 && code < letterCount - 1) {
			Node* next = children[code];
			if (next)
				return next->search(word + 1);
		}
	}
	return false;
}

void Node::printWords() {
	std::string empty;
	printWords(empty);
	std::cout << std::endl;
}

void Node::printWords(const std::string& lettersUpToNow) {
	if (children[Node::letterCount - 1]) {
		std::cout << lettersUpToNow << ", ";
	}
	for (int i = 0; i < Node::letterCount - 1; ++i) {
		Node* next = children[i];
		if (next) {
			next->printWords(lettersUpToNow + std::string(1, 'a' + i));
		}
	}
}
