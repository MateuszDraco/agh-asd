#include <iostream>

class Node {
public:
	static Node Empty;
	static const int letterCount = 27;

	typedef Node* NodePtr;
	NodePtr children[letterCount];

	Node() {
		for (int i = 0; i < letterCount; ++i)
			children[i] = 0;
	}
	~Node() {
		for (int i = 0; i < letterCount - 1; ++i)
			delete children[i];
	}
};

Node Node::Empty;

void add(Node& current, const char* word) {
	if (word[0] == '\0') {
		current.children[26] = &Node::Empty;
	}
	else {
		Node* next = current.children[word[0] - 'a'];
		if (!next) {
			next = new Node();
			current.children[word[0] - 'a'] = next;
		}
		add(*next, word + 1);
	}
}

void remove(Node& current, const char* word) {
	if (word[0] == '\0') {
		current.children[Node::letterCount - 1] = 0;
	}
	else {
		int keyCode = word[0] - 'a';
		Node* next = current.children[keyCode];
		if (!next)
			return;
		remove(*next, word + 1);
		bool removeNext = true;
		for (int i = 0; i < Node::letterCount && removeNext; ++i)
			removeNext = removeNext && (next->children[i] == 0);
		if (removeNext) {
			current.children[keyCode] = 0;
			delete next;
		}
	}
}

void printChar(char c, int indent) {
	for (int i = 0; i < indent; ++i)
		std::cout << "  ";
	std::cout << c << std::endl;
}

void show(Node& current, int indent) {
	if (current.children[Node::letterCount - 1]) {
		printChar('.', indent);
	}
	for (int i = 0; i < Node::letterCount - 1; ++i) {
		Node* next = current.children[i];
		if (next) {
			printChar('a' + i, indent);
			show(*next, indent + 1);
		}
	}
}

int main() {
	Node root;
	add(root, "makaron");
	add(root, "maslo");
	add(root, "mikroskop");
	add(root, "mikro");
	add(root, "mak");
	add(root, "maka");
	add(root, "maki");
	std::cout << "dictionary: " << std::endl;
	show(root, 0);
	remove(root, "mikroskop");
	remove(root, "makaron");
	std::cout << "dictionary: " << std::endl;
	show(root, 0);
	return 0;
}