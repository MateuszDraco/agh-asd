#include <string>

class Node {
public:
	/// pusty element
	static Node Empty;
	/// ilosc liter
	static const int letterCount = 27;

	// konstruktor
	Node();
	// destruktor
	~Node();

	// dodaje słowo
	void add(const char* word);
	// usuwa słowo
	void remove(const char* word);
	// wyszukuje czy słowo jest w słowniku
	bool search(const char* word);

	// wyświetla słownik
	void print();

	// wyswietla slowa
	void printWords();
private:
	int charCode(char c);
	void print(int indent);
	void printWords(const std::string& lettersUpToNow);

	Node* children[letterCount];
};
