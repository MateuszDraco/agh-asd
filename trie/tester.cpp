#include "trie.h"
#include <iostream>

void show(Node& root) {
	std::cout << "dictionary: " << std::endl;
	root.print();
	std::cout << "words inside: ";
	root.printWords();
}

int main() {
	Node root;
	root.add("makaron");
	root.add("maslo");
	root.add("mikroskop");
	root.add("mikro");
	root.add("mak");
	root.add("maka");
	root.add("maki");

	show(root);
	std::cout << "search for makaron: " << root.search("makaron") << std::endl;

	std::cout << "removing mikroskop and makaron..." << std::endl << std::endl;
	root.remove("mikroskop");
	root.remove("makaron");

	show(root);
	std::cout << "search for makaron: " << root.search("makaron") << std::endl;

	return 0;
}


